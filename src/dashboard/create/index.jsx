import React from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography/Typography";
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper/Paper";
import TextField from "@material-ui/core/TextField/TextField";
import InvoiceStore from "../../store/InvoiceStore";
import Divider from "@material-ui/core/Divider/Divider";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import Button from "@material-ui/core/Button/Button";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import ConfirmationDialog from "../../components/confirm-dialog";
import UiInvoiceStore from "../../store/UiInvoiceStore";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit,
        color: theme.palette.text.secondary,
    },
    subHeading: {
        marginBottom: 15
    },
    textField: {
        lineHeight: 30,
        padding: 0
    },
    leftMargin: {
        marginLeft: 15
    },
    rightMargin: {
        marginRight: 15
    },
    shipToCopy: {
        marginBottom: 13
    },
    pointerDisabled: {
        pointerEvents: "none"
    }
});

@observer
class CreateInvoice extends React.Component {

    shipToCheckChange = (e) => {
        UiInvoiceStore.changeShipToChecked(e.target.checked);
        InvoiceStore.copyShipTo();
    };
    confirmDialogOk = () => {
        InvoiceStore.resetModel();
        UiInvoiceStore.resetUi();
        this.toggleConfirmDialog();
    };

    confirmDialogCancel = () => {
        this.toggleConfirmDialog();
    };

    toggleConfirmDialog = () => {
        this.setState({confirmationDialogOpen: !this.state.confirmationDialogOpen});
    };

    constructor(props) {
        super(props);
        this.state = {
            confirmationDialogOpen: false
        };
    }

    render() {
        const {classes} = this.props;
        let invoice = InvoiceStore.invoice;
        return (<div>
                <ConfirmationDialog
                    open={this.state.confirmationDialogOpen}
                    title="!! warning !!"
                    message="Are you sure want to close? All the data you have entered will be lost."
                    okFunc={this.confirmDialogOk}
                    cancelFunc={this.confirmDialogCancel}
                />
                <Typography variant="h5" gutterBottom>
                    Create Invoice
                </Typography>
                <form className={classes.container} noValidate autoComplete="off">
                    <Paper className={classes.paper}>
                        <Typography variant="h6" className={classes.subHeading}>
                            Basic info
                        </Typography>
                        <Grid container spacing={24}>
                            <Grid item xs={6}>
                                <TextField
                                    label="Invoice No." className={classes.textField} value={invoice.invoiceNo}
                                    margin="dense" fullWidth={true} variant="outlined" disabled/>

                                <TextField
                                    label="Reverse Charge" className={classes.textField} value={invoice.reverseCharge}
                                    onChange={(e) => InvoiceStore.updateInvoice('reverseCharge', e.target.value)}
                                    margin="dense" fullWidth={true} variant="outlined"/>

                                <TextField
                                    label="Invoice Date" className={classes.textField} value={invoice.date}
                                    fullWidth={true}
                                    margin="dense" variant="outlined" disabled/>

                                <Grid container spacing={24}>
                                    <Grid item xs={8}>
                                        <TextField
                                            label="State" className={classes.textField + "" + classes.rightMargin}
                                            value={invoice.address.state}
                                            onChange={(e) => InvoiceStore.updateInvoice('address.state', e.target.value)}
                                            margin="dense" fullWidth={true} variant="outlined"/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <TextField
                                            label="Code" type="number"
                                            className={classes.textField + "" + classes.smallTextField}
                                            value={invoice.address.code}
                                            onChange={(e) => InvoiceStore.updateInvoice('address.code', e.target.value)}
                                            margin="dense" fullWidth={true} variant="outlined"/>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label="Transportation mode" className={classes.textField}
                                    value={invoice.transportationMode}
                                    onChange={(e) => InvoiceStore.updateInvoice('transportationMode', e.target.value)}
                                    margin="dense" fullWidth={true} variant="outlined"/>

                                <TextField label="Vehicle number" className={classes.textField}
                                           value={invoice.vehicleNumber}
                                           onChange={(e) => InvoiceStore.updateInvoice('vehicleNumber', e.target.value)}
                                           margin="dense" fullWidth={true} variant="outlined"/>
                                <TextField label="Date of supply" className={classes.textField}
                                           value={invoice.dateOfSupply}
                                           onChange={(e) => InvoiceStore.updateInvoice('dateOfSupply', e.target.value)}
                                           margin="dense" fullWidth={true} variant="outlined"/>

                                <TextField label="Place of supply" className={classes.textField}
                                           value={invoice.placeOfSupply}
                                           onChange={(e) => InvoiceStore.updateInvoice('placeOfSupply', e.target.value)}
                                           margin="dense" fullWidth={true} variant="outlined"/> </Grid>
                        </Grid>
                    </Paper>
                    <br/>
                    <Paper className={classes.paper}>
                        <Grid container spacing={24}>
                            <Grid item xs={6}>
                                <Typography variant="h6" className={classes.subHeading}>
                                    Bill of party
                                </Typography>
                                <TextField
                                    label="Name" className={classes.textField}
                                    value={invoice.billTo.name}
                                    onChange={(e) => InvoiceStore.updateInvoice('billTo.name', e.target.value)}
                                    margin="dense" fullWidth={true} variant="outlined"/>

                                <TextField label="Address" className={classes.textField}
                                           value={invoice.billTo.line1}
                                           onChange={(e) => InvoiceStore.updateInvoice('billTo.line1', e.target.value)}
                                           margin="dense" fullWidth={true} variant="outlined"/>
                                <TextField label="GSTIN" className={classes.textField} value={invoice.billTo.gstin}
                                           onChange={(e) => InvoiceStore.updateInvoice('billTo.gstin', e.target.value)}
                                           margin="dense" fullWidth={true} variant="outlined"/>
                                <TextField label="Royalty/Ravanna No." className={classes.textField}
                                           value={invoice.billTo.royaltyNo}
                                           onChange={(e) => InvoiceStore.updateInvoice('billTo.royaltyNo', e.target.value)}
                                           margin="dense" fullWidth={true} variant="outlined"/>
                                <Grid container spacing={24}>
                                    <Grid item xs={8}>
                                        <TextField
                                            label="State" className={classes.textField + "" + classes.rightMargin}
                                            value={invoice.billTo.state}
                                            onChange={(e) => InvoiceStore.updateInvoice('billTo.state', e.target.value)}
                                            margin="dense" fullWidth={true} variant="outlined"/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <TextField
                                            label="Code" type="number"
                                            className={classes.textField + "" + classes.smallTextField}
                                            value={invoice.billTo.code}
                                            onChange={(e) => InvoiceStore.updateInvoice('billTo.code', e.target.value)}
                                            margin="dense" fullWidth={true} variant="outlined"/>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography variant="h6" className={classes.subHeading}>
                                    Ship to party
                                </Typography>
                                <FormControlLabel className={classes.shipToCopy}
                                                  control={
                                                      <Checkbox
                                                          checked={UiInvoiceStore.shipToChecked}
                                                          onChange={this.shipToCheckChange}
                                                          value="checked"
                                                          color="primary"
                                                      />
                                                  }
                                                  label="Same as bill of party"
                                />
                                <br/>
                                <div className={UiInvoiceStore.shipToChecked ? classes.pointerDisabled : ""}>
                                    <TextField
                                        label="Name" className={classes.textField}
                                        value={invoice.shipTo.name}
                                        onChange={(e) => InvoiceStore.updateInvoice('shipTo.name', e.target.value)}
                                        margin="dense" fullWidth={true} variant="outlined"/>

                                    <TextField label="Address" className={classes.textField}
                                               value={invoice.shipTo.line1}
                                               onChange={(e) => InvoiceStore.updateInvoice('shipTo.line1', e.target.value)}
                                               margin="dense" fullWidth={true} variant="outlined"/>
                                    <TextField label="GSTIN" className={classes.textField} value={invoice.shipTo.gstin}
                                               onChange={(e) => InvoiceStore.updateInvoice('shipTo.gstin', e.target.value)}
                                               margin="dense" fullWidth={true} variant="outlined"/>
                                    <Grid container spacing={24}>
                                        <Grid item xs={8}>
                                            <TextField
                                                label="State" className={classes.textField + "" + classes.rightMargin}
                                                value={invoice.shipTo.state}
                                                onChange={(e) => InvoiceStore.updateInvoice('shipTo.state', e.target.value)}
                                                margin="dense" fullWidth={true} variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={4}>
                                            <TextField
                                                label="Code" type="number"
                                                className={classes.textField + "" + classes.smallTextField}
                                                value={invoice.shipTo.code}
                                                onChange={(e) => InvoiceStore.updateInvoice('shipTo.code', e.target.value)}
                                                margin="dense" fullWidth={true} variant="outlined"/>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                        </Grid>
                    </Paper>
                    <br/>
                    <Paper className={classes.paper}>
                        <Typography variant="h6" className={classes.subHeading}>
                            Products Description
                        </Typography>
                        {
                            invoice.invoiceLines.map((invoiceLine, index) => {
                                return <div style={{marginTop: index !== 0 ? 10 : "auto"}}>
                                    <Typography variant="caption">
                                        Product - {index + 1}
                                    </Typography>
                                    <Grid container spacing={24} key={index}>
                                        <Grid item xs={10}>
                                            <Grid container spacing={24}>
                                                <Grid item xs={10}>
                                                    <TextField
                                                        label="Description of Goods" className={classes.textField}
                                                        value={invoiceLine.description}
                                                        onChange={(e) => InvoiceStore.updateInvoiceLine(index, 'description', e.target.value)}
                                                        margin="dense" fullWidth={true} variant="outlined"/>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <TextField
                                                        label="HSN Code" className={classes.textField}
                                                        value={invoiceLine.hsnCode}
                                                        onChange={(e) => InvoiceStore.updateInvoiceLine(index, 'hsnCode', e.target.value)}
                                                        margin="dense" fullWidth={true} variant="outlined"/>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={24}>
                                                <Grid item xs={2}>
                                                    <TextField
                                                        label="Size" className={classes.textField}
                                                        value={invoiceLine.size}
                                                        onChange={(e) => InvoiceStore.updateInvoiceLine(index, 'size', e.target.value)}
                                                        margin="dense" fullWidth={true} variant="outlined"/>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <TextField
                                                        label="No. of Pieces" className={classes.textField}
                                                        value={invoiceLine.pieces}
                                                        onChange={(e) => InvoiceStore.updateInvoiceLine(index, 'pieces', e.target.value)}
                                                        margin="dense" fullWidth={true} variant="outlined"/>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <Grid container spacing={24}>
                                                        <Grid item xs={8}>
                                                            <TextField
                                                                label="Sqf./Sqm" className={classes.textField}
                                                                value={invoiceLine.measurement}
                                                                onChange={(e) => InvoiceStore.updateInvoiceLine(index, 'measurement', e.target.value)}
                                                                margin="dense" fullWidth={true} variant="outlined"/>
                                                        </Grid>
                                                        <Grid item xs={4}>
                                                            <FormControl className={classes.formControl}>
                                                                <InputLabel htmlFor="sqf-sqm">Type</InputLabel>
                                                                <Select
                                                                    value={invoiceLine.measurementType}
                                                                    onChange={(e, value) => InvoiceStore.updateInvoiceLine(index, 'measurementType', e.target.value)}
                                                                    inputProps={{
                                                                        name: 'measurementType',
                                                                        id: 'sqf-sqm',
                                                                    }}>
                                                                    <MenuItem value="sqf">Sqf</MenuItem>
                                                                    <MenuItem value="sqm">Sqm</MenuItem>
                                                                </Select>
                                                            </FormControl>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <TextField
                                                        label="Rate" className={classes.textField}
                                                        value={invoiceLine.rate}
                                                        onChange={(e) => InvoiceStore.updateInvoiceLine(index, 'rate', e.target.value)}
                                                        margin="dense" fullWidth={true} variant="outlined"
                                                        type="number"/>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <TextField
                                                        label="Amount (Rs.)" className={classes.textField}
                                                        value={invoiceLine.amount}
                                                        onChange={(e) => InvoiceStore.updateInvoiceLine(index, 'amount', e.target.value)}
                                                        margin="dense" fullWidth={true} variant="outlined"
                                                        type="number"/>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Button color="secondary" disabled={invoice.invoiceLines.length === 1}
                                                    onClick={() => InvoiceStore.removeInvoiceLine(index)}>
                                                Remove
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </div>
                            })
                        }
                        <div style={{textAlign: "right", marginTop: 5}}>
                            <Button color="primary" onClick={InvoiceStore.addInvoiceLine}>Add Product</Button>
                        </div>
                    </Paper>
                    <br/>
                    <Paper className={classes.paper}>
                        <div>
                            <Typography variant="h6" className={classes.subHeading}>
                                Calculations
                            </Typography>
                            <Grid container spacing={24}>
                                <Grid item xs={2}>
                                    <TextField
                                        label="Total amount" className={classes.textField}
                                        disabled={true}
                                        value={InvoiceStore.getTotalInvoiceLineAmount} type="number"
                                        margin="dense" fullWidth={true} variant="outlined"/>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField label="Royalty" className={classes.textField}
                                               value={invoice.pricing.royalty} type="number"
                                               onChange={(e) => InvoiceStore.updateInvoice('pricing.royalty', e.target.value)}
                                               margin="dense" fullWidth={true} variant="outlined"/>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField label="Loading" className={classes.textField}
                                               value={invoice.pricing.loading}
                                               onChange={(e) => InvoiceStore.updateInvoice('pricing.loading', e.target.value)}
                                               margin="dense" fullWidth={true} variant="outlined" type="number"/>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField label="Packing" className={classes.textField}
                                               value={invoice.pricing.packing}
                                               onChange={(e) => InvoiceStore.updateInvoice('pricing.packing', e.target.value)}
                                               margin="dense" fullWidth={true} variant="outlined" type="number"/>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField
                                        label="Total amount before tax" className={classes.textField}
                                        value={invoice.pricing.priceBeforeTax}
                                        disabled={true}
                                        margin="dense" fullWidth={true} variant="outlined" type="number"/>
                                </Grid>
                            </Grid>
                            <Grid container spacing={24}>
                                <Grid item xs={2}>
                                    <TextField
                                        label="CGST" className={classes.textField}
                                        value={invoice.pricing.gst.cgst} type="number"
                                        onChange={(e) => InvoiceStore.updateInvoice('pricing.gst.cgst', e.target.value)}
                                        disabled={UiInvoiceStore.editTaxesDisabled}
                                        margin="dense" fullWidth={true} variant="outlined"/>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField label="SGST" className={classes.textField}
                                               value={invoice.pricing.gst.sgst} type="number"
                                               onChange={(e) => InvoiceStore.updateInvoice('pricing.gst.sgst', e.target.value)}
                                               disabled={UiInvoiceStore.editTaxesDisabled}
                                               margin="dense" fullWidth={true} variant="outlined"/>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField label="IGST" className={classes.textField}
                                               value={invoice.pricing.gst.igst} type="number"
                                               onChange={(e) => InvoiceStore.updateInvoice('pricing.gst.igst', e.target.value)}
                                               disabled={UiInvoiceStore.editTaxesDisabled}
                                               margin="dense" fullWidth={true} variant="outlined"/>
                                </Grid>
                                <Grid item xs={2}>
                                    <Button color="primary"
                                            onClick={UiInvoiceStore.toggleEditTaxesDisable}> {UiInvoiceStore.editTaxesDisabled ? "Edit" : "Save"} </Button>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField label="Total Tax Amount (GST)" className={classes.textField}
                                               value={invoice.pricing.gst.total} type="number" disabled="true"
                                               onChange={(e) => InvoiceStore.updateInvoice('pricing.gst.total', e.target.value)}
                                               margin="dense" fullWidth={true} variant="outlined"/>
                                </Grid>
                            </Grid>
                            <Grid container spacing={24}>
                                <Grid item xs={2}>
                                    <Typography variant="subheading" gutterBottom>
                                        Grand Total After taxes :
                                    </Typography>
                                </Grid>
                                <Grid item xs={3}>
                                    <Typography variant="title" gutterBottom>
                                        2,34,000
                                    </Typography>
                                </Grid>
                                <Grid item xs={3}>
                                    <TextField
                                        label="GST on reverse charge" type="number"
                                        className={classes.textField + "" + classes.smallTextField}
                                        value={invoice.reverseCharge}
                                        onChange={(e) => InvoiceStore.updateInvoice('reverseCharge', e.target.value)}
                                        margin="dense" fullWidth={true} variant="outlined"/>
                                </Grid>
                            </Grid>
                        </div>
                    </Paper>
                    <br/>
                    <div style={{textAlign: "right"}}>
                        <Button variant="contained"
                                onClick={this.toggleConfirmDialog}>Close</Button>
                        <Button variant="contained" color="primary" className={classes.leftMargin}
                                disabled={UiInvoiceStore.invoiceSaved}
                                onClick={InvoiceStore.saveInvoice}>Save</Button>
                        <Button variant="contained" color="secondary" className={classes.leftMargin}
                                disabled={!UiInvoiceStore.invoiceSaved}
                                onClick={InvoiceStore.addInvoiceLine}>Print</Button>
                    </div>
                </form>
            </div>
        );
    }
}

CreateInvoice.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateInvoice);
