import React from 'react';
import IndexedDB from '../../service/indexedDB.js';
import InvoiceStore from '../../store/InvoiceStore';
import { observer } from 'mobx-react';

@observer
class ViewInvoice extends React.Component {
    constructor(props) {
        super(props);
        IndexedDB.clearDataBase();
        console.log(InvoiceStore);
        setTimeout(() => {
            InvoiceStore.invoice.invoiceNo = 100;
        }, 1000);
    }

    render() {
        return (<div>
            <h2>Welcome to View Invoice! {InvoiceStore.invoice.invoiceNo}</h2>
        </div>);
    }
}

export default ViewInvoice;
