/**
 * Updates form
 * @params {form} Form to be updated
 * @params {path} Path to be updated
 * @params {value} Updated value
 */
export function updateObjectProperty(form, path, value) {
    let schema = form; // a moving reference to internal objects within obj
    const pList = path.split('.');
    const len = pList.length;
    for (let i = 0; i < len - 1; i += 1) {
        const elem = pList[i];
        if (!schema[elem]) {
            schema[elem] = {};
        }
        schema = schema[elem];
    }

    schema[pList[len - 1]] = value;
}
