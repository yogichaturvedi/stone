import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {createMuiTheme, MuiThemeProvider, withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {HashRouter as Router, Link, Route} from "react-router-dom";
import ListInvoice from "./dashboard/list";
import CreateInvoice from "./dashboard/create";
import ViewInvoice from "./dashboard/view";
import MenuList from "@material-ui/core/MenuList/MenuList";
import {blue} from "@material-ui/core/colors";
import Menu from "@material-ui/core/Menu/Menu";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ExportInvoice from "./dashboard/export/index";
import ImportInvoice from "./dashboard/import/index";

const drawerWidth = 240;
const theme1 = createMuiTheme({
    palette: {
        primary: blue
    },
    typography: {
        useNextVariants: true,
    }
});
const styles = theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    menuItem: {
        color: theme.palette.common.white,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        overflow: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9,
        },
    },
    toolbar: {
        ...theme.mixins.toolbar,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
    },
    fullWidth: {
        width: "100%"
    },
    accountButton: {
        marginLeft: 36,
        marginRight: 12
    }
});

class AppRouter extends React.Component {

    state = {
        openDrawer: false,
        selectedMenu: 'List',
        anchorEl: null
    };

    toggleDrawer = () => {
        this.setState({openDrawer: !this.state.openDrawer});
    };

    onMenuClicked = (selectedItem) => {
        this.setState({selectedMenu: selectedItem.title});
        // this.props.history.push(selectedItem.link)
    };

    handleAccountMenuOpen = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleAccountMenuClose = () => {
        this.setState({anchorEl: null});
    };

    render() {
        const {classes, theme} = this.props;
        const {anchorEl} = this.state;
        const isAccountMenuOpen = Boolean(anchorEl);

        let menuItems = [
            {title: 'Create', image: 'create', link: "/create"},
            {title: 'List', image: 'list', link: "/"},
            {title: 'Import', image: 'import', link: "/import"},
            {title: 'Export', image: 'export', link: "/export"}
        ];
        return (
            <MuiThemeProvider theme={theme1}>
                <Router>
                    <div className={classes.root}>
                        <CssBaseline/>
                        <AppBar
                            position="fixed"
                            className={classNames(classes.appBar, {
                                [classes.appBarShift]: this.state.openDrawer,
                            })}>
                            <Toolbar disableGutters={true}>
                                <IconButton
                                    color="inherit"
                                    aria-label="Open drawer"
                                    onClick={this.toggleDrawer}
                                    className={classNames(classes.menuButton)}>
                                    {
                                        this.state.openDrawer ? <ArrowBack/> : <MenuIcon/>
                                    }

                                </IconButton>
                                <Typography variant="h5" color="inherit" className={classes.fullWidth} noWrap>
                                    Stone
                                </Typography>

                                <div>
                                    <IconButton
                                        aria-owns={isAccountMenuOpen ? 'menu-appbar' : undefined}
                                        aria-haspopup="true"
                                        onClick={this.handleAccountMenuOpen}
                                        color="inherit"
                                        className={classes.accountButton}
                                    >
                                        <AccountCircle/>
                                    </IconButton>
                                    <Menu
                                        id="menu-appbar"
                                        anchorEl={anchorEl}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={isAccountMenuOpen}
                                        onClose={this.handleAccountMenuClose}
                                    >
                                        <MenuItem onClick={this.handleAccountMenuClose}>Logout</MenuItem>
                                        {/*<MenuItem onClick={this.handleClose}>My account</MenuItem>*/}
                                    </Menu>
                                </div>

                            </Toolbar>
                        </AppBar>
                        <Drawer
                            variant="permanent"
                            classes={{
                                paper: classNames(classes.drawerPaper, !this.state.openDrawer && classes.drawerPaperClose),
                            }}
                            open={this.state.openDrawer}>
                            <div className={classes.toolbar}>
                            </div>
                            <Divider/>
                            <MenuList component="nav">
                                {menuItems.map((item, index) => (
                                    <Link to={item.link} key={index} className="noTextDecoration">
                                        <ListItem button key={index} onClick={() => this.onMenuClicked(item)}
                                                  selected={this.state.selectedMenu === item.title}
                                                  className={this.state.selectedMenu === item.title ? "selected-menu" : ""}>
                                            <ListItemIcon>
                                                <img
                                                    src={"./assets/icons/" + item.image + (this.state.selectedMenu === item.title ? "-white.png" : "-default.png")}/>
                                            </ListItemIcon>
                                            <ListItemText>{item.title}</ListItemText>
                                        </ListItem>
                                    </Link>
                                ))}
                            </MenuList>
                        </Drawer>
                        <main className={classes.content + " app-container"}>
                            <div className={classes.toolbar}/>
                            <Route path="/" exact component={ListInvoice}/>
                            <Route path="/create/" component={CreateInvoice}/>
                            <Route path="/view/" component={ViewInvoice}/>
                            <Route path="/import/" component={ImportInvoice}/>
                            <Route path="/export/" component={ExportInvoice}/>
                        </main>
                    </div>
                </Router>
            </MuiThemeProvider>
        );
    }
}

AppRouter.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(AppRouter);
