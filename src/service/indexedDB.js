// import _ from 'lodash';
import DB from '../database-schema';
import {toJS} from "mobx";

class IndexedDB {
    clearDataBase() {
        return new Promise(async (resolve, reject) => {
            const response = {
                data: [],
            };
            try {
                const tables = DB.tables;
                for (let i = 0; i < tables.length; i += 1) {
                    // if (tables[i].name !== 'AuthData' && tables[i].name !== 'Customers') {
                    // eslint-disable-next-line
                    const clearTableResponse = await tables[i].clear();
                    response.data.push(clearTableResponse);
                    // }
                }
                resolve(response);
            }
            catch (error) {
                reject(error);
            }
        });
    }

    async addInvoice(invoice) {
      return DB.Invoice.add(toJS(invoice)).then(()=>{
          console.log("success");

      }).catch((e)=>{
          console.log("error",e);
        });
    }

    getInvoices = async () => {
        return await DB.Invoice.toArray(data => {
            return data;
        });
    };

    countInvoice() {
        return DB.Invoice.count().then((data)=>{
            console.log("COUNT----->",data);
            return data;

        }).catch((e)=>{
            console.log("error in fetching count for invoice",e);
        });
    }

}

export default new IndexedDB;
