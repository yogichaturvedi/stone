import Dexie from 'dexie';

const DB = new Dexie('Jobs');

DB.on('versionchange', () => {
    if (window.location) {
        window.location.reload();
    }
});

DB.version(1)
    .stores({
        Invoice: '++invoiceNo, dateOfSupply',
    });

export default DB;
