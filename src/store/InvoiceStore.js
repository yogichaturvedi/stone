import Invoice from '../model/invoice';
import {updateObjectProperty} from "../utils";
import InvoiceLine from "../model/invoice-line";
import UiInvoiceStore from "../store/UiInvoiceStore";
import IndexedDB from "../service/indexedDB";
import {observable, toJS} from "mobx";

class InvoiceStore {
    @observable invoices = [];

    copyShipTo = () => {
        this.invoice.shipTo.name = this.invoice.billTo.name;
        this.invoice.shipTo.line1 = this.invoice.billTo.line1;
        this.invoice.shipTo.gstin = this.invoice.billTo.gstin;
        this.invoice.shipTo.state = this.invoice.billTo.state;
        this.invoice.shipTo.code = this.invoice.billTo.code;
    };

    addInvoiceLine = () => this.invoice.invoiceLines.push(new InvoiceLine());

    removeInvoiceLine = index => this.invoice.invoiceLines.splice(index, 1);

    generateInvoiceNumber = async () => {
        this.invoice.invoiceNo = await IndexedDB.countInvoice() + 1;
    };

    updateInvoice = (path, value) => {
        updateObjectProperty(this.invoice, path, value);
        if (UiInvoiceStore.shipToChecked && path.split(".")[0] === "billTo") {
            updateObjectProperty(this.invoice, "shipTo." + path.split(".")[1], value);
        }
    };

    updateInvoiceLine = (index, key, value) => {
        this.invoice.invoiceLines[index][key] = value;
    };

    getTotalInvoiceLineAmount = () => {
        return "";
    };

    resetModel = () => {
        this.invoice = new Invoice();
        this.generateInvoiceNumber();
        this.invoice.date = new Date().toLocaleDateString();
        this.addInvoiceLine();
    };

    saveInvoice = async () => {
        UiInvoiceStore.changeInvoiceSaved(true);
        try {
            await IndexedDB.addInvoice(this.invoice);
            let data = await IndexedDB.getInvoice();
            console.log(data);
        } catch (e) {
            console.log("Error in saveInvoice func", e);
        }
    };

    fetchInvoices = async () => {
        this.invoices = await IndexedDB.getInvoices();
        console.log(this.invoices);
    };

    constructor() {
        this.resetModel();
        // IndexedDB.getInvoices();
    }
}

export default new InvoiceStore();
