import {observable} from 'mobx';

class UiInvoiceStore {
    @observable shipToChecked = false;
    @observable editTaxesDisabled = true;
    @observable invoiceSaved = false;

    changeShipToChecked = (value) =>
        this.shipToChecked = value;

    toggleEditTaxesDisable = () => {
        this.editTaxesDisabled = !this.editTaxesDisabled;
    };

    changeInvoiceSaved = (value) => {
        this.invoiceSaved = value;
    };

    resetUi = () => {
        this.shipToChecked = false;
        this.editTaxesDisabled = true;
        this.invoiceSaved = false;
    };

    constructor() {
        this.resetUi();
    }
}

export default new UiInvoiceStore();
