import {observable} from "mobx";

class InvoiceLine {
    @observable description = "";
    @observable hsnCode = "";
    @observable size = "";
    @observable pieces = "";
    @observable measurementType = "sqf";
    @observable measurement = "";
    @observable rate = "";
    @observable amount = "";
}

export default InvoiceLine;
