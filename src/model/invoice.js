import {observable} from 'mobx';

class Invoice {
    @observable invoiceNo = "";
    @observable id = "";
    @observable date = "";
    @observable reverseCharge = "";
    @observable transportationMode = "";
    @observable vehicleNumber = "";
    @observable dateOfSupply = "";
    @observable placeOfSupply = "";
    @observable address = {
        line1: "",
        state: "",
        city: "",
        code: ""
    };
    @observable  billTo = {
        name: "",
        gstin: "",
        line1: "",
        state: "",
        city: "",
        code: "",
        royaltyNo: ""
    };
    @observable  shipTo = {
        name: "",
        gstin: "",
        line1: "",
        state: "",
        city: "",
        code: ""
    };
    @observable invoiceLines = [];
    @observable pricing = {
        priceBeforeTax: 0,
        gst: {
            cgst: 2.5,
            sgst: 2.5,
            igst: 5,
            total: 10
        },
        totalTax: 0,
        royalty:"",
        loading : "",
        packing:"",
        priceAfterTax: 0,
        total: 0
    };
    @observable  bankDetails = {
        name: "",
        accountNo: "",
        branch: "",
        ifscCode: ""
    }
}

export default Invoice;
